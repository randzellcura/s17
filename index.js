/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function getUserInfo() {
        let firstName = prompt("Enter first name here: ");
        let lastName = prompt("Enter last name here: ");
        let age = prompt("Enter age here: ");
        let address = prompt("Enter address here: ");
        alert("Thanks for your response!")
        console.log(firstName + " " + lastName + ", aged " + age + ", who lives at " + address);
    };
    getUserInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function favArtists() {
        console.log("1. Niki");
        console.log("2. Billie Eilish");
        console.log("3. Lauv");
        console.log("4. Joji");
        console.log("5.Keshi");
    };
    favArtists();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function topFiveMovies () {
        console.log("1. Evangelion: 3.0 + 1.01 Thrice Upon a Time");
        console.log("Rotten Tomatos Rating: 100%");
        console.log("2. JOSEE, THE TIGER AND THE FISH");
        console.log("Rotten Tomatos Rating: 100%");
        console.log("3. PRETTY GUARDIAN SAILOR MOON ETERNAL THE MOVIE");
        console.log("Rotten Tomatos Rating: 100%");
        console.log("4. VIOLET EVERGARDEN: THE MOVIE");
        console.log("Rotten Tomatos Rating: 100%");
        console.log("5. WORDS BUBBLE UP LIKE SODA POP");
        console.log("Rotten Tomatos Rating: 100%");
    }
    topFiveMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function () {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);